package com.sm.task.smee.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;


/**
 * This bean is a simplest POJO bean without persistence related annotations.
 * With real database backend we would probably annotate it with min. @Id
 * 
 * Equals and HashCode methods were overriden as a good practice
 * 
 * @author kris
 *
 */
@XmlRootElement
public class LogEntry implements Serializable {
    /** Default value included to remove warning. Remove or modify at will. **/
    private static final long serialVersionUID = 1L;

    private Long id;
    private String content;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * We could make the first validation as min -> max values but I added on purpose another validation for 
	 * more 'sophisticated' unit tests :)
	 * 
	 * @return
	 */
	@Size(min=1, max = 160, message="Maximum 160 chars")
	@NotEmpty(message="Content for Log Entry cannot be empty")
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogEntry other = (LogEntry) obj;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "LogEntry [id=" + id + ", content=" + content + "]";
	}
	
}