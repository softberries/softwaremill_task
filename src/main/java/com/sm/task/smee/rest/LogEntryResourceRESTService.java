package com.sm.task.smee.rest;

import com.sm.task.smee.model.LogEntry;
import com.sm.task.smee.dao.LogEntryDao;
import com.sm.task.smee.data.LogEntryRepository;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * JAX-RS Example
 * 
 * This class produces a RESTful service to read the contents of the members table.
 */
@Path("/logentries")
@RequestScoped
public class LogEntryResourceRESTService {
    @Inject
    @LogEntryRepository
    private LogEntryDao logEntryDao;

    @GET
    @Produces("application/xml")
    public List<LogEntry> listAllMembers() {
        // Use @SupressWarnings to force IDE to ignore warnings about "genericizing" the results of this query
        @SuppressWarnings("unchecked")
        // We recommend centralizing inline queries such as this one into @NamedQuery annotations on the @Entity class
        // as described in the named query blueprint: https://blueprints.dev.java.net/bpcatalog/ee5/persistence/namedquery.html
        final List<LogEntry> results = logEntryDao.getAllLogEntries();
        return results;
    }

    @GET
    @Path("/{id:[1-9][0-9]*}")
    @Produces("application/xml")
    public LogEntry lookupMemberById(@PathParam("id") int id) {
    	LogEntry result = logEntryDao.getAllLogEntries().get(id);
        return result;
    }
}
