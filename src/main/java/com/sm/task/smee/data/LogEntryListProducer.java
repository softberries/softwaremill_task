package com.sm.task.smee.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.jboss.seam.solder.logging.Category;

import com.sm.task.smee.dao.LogEntryDao;
import com.sm.task.smee.model.LogEntry;

@RequestScoped
public class LogEntryListProducer {
	
    @Inject
    @LogEntryRepository
    private LogEntryDao logEntryDao;

    @Inject
    @Category("smee")
    private Logger log;
    
    private List<LogEntry> logEntries;

    //this way we can use the list directly on the view layer
    @Produces
    @Named
    public List<LogEntry> getLogEntries() {
        return logEntries;
    }

    /**
     * Observer pattern impl.
     * This method is called when the new log entries {@link LogEntry} are added to the list
     * @param entry
     */
    public void onLogEntryListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final LogEntry entry) {
    	log.info("observer method called: " + entry);
    	//updates currently displayed list of log entries
        retrieveAllLogEntriesOrderedByName();
    }

    @PostConstruct
    public void retrieveAllLogEntriesOrderedByName() {
    	logEntries = logEntryDao.getAllLogEntries();
    }
}
