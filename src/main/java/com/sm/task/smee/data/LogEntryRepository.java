package com.sm.task.smee.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * A qualifier used to differentiate between multiple data repositories.
 * 
 * I'm going to use just a session scoped bean for this example which will simulate
 * the persistent store (of course just for the duration of the session)
 */
@Qualifier
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface LogEntryRepository {
    /* class body intentionally left blank */
}
