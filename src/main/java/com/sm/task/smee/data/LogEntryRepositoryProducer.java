package com.sm.task.smee.data;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.sm.task.smee.dao.LogEntryDao;

/**
 * This class uses CDI to produce {@code LogEntryDao} instances qualified that are qualified as &#064;LogEntryRepository.
 * <pre>
 * &#064;Inject
 * &#064;LogEntryRepository
 * </pre>
 */
public class LogEntryRepositoryProducer {
	
	@Inject
	private LogEntryDao logEntryDao;
	
    @Produces
    @LogEntryRepository
    public LogEntryDao createDao(){
    	return logEntryDao;
    }
}
