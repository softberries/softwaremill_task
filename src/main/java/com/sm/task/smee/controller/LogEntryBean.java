package com.sm.task.smee.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.jboss.seam.solder.logging.Category;

import com.sm.task.smee.dao.LogEntryDao;
import com.sm.task.smee.data.LogEntryRepository;
import com.sm.task.smee.model.LogEntry;

/**
 * Managed bean responsible for interaction with the view
 * 
 * Adding the @Stateful annotation eliminates need for manual transaction demarcation
 * @{code @javax.ejb.Stateful}
 * I'm not going to use it as it would require to shift to full EE profile
 * 
 * Actually I'm not going to use any transaction, instead of implicit EJB transactions we could of course
 * use {@code UserTransaction} but for the purpose of this assignment I will skip it.
 * @author kris
 *
 */
@Model
public class LogEntryBean {
	
    @Inject
    @Category("smee")
    private Logger log;

    /**
     * Object responsible for interaction with persistence store
     */
    @Inject
    @LogEntryRepository
    private LogEntryDao logEntryDao;

    /**
     * Event generated when the new log entry is added
     */
    @Inject
    private Event<LogEntry> logEntryEventSrc;

    /**
     * After its produced, this object is attached to the JSF view
     */
    private LogEntry logEntry;

    /**
     * Factory for new log entries
     * @return
     */
    @Produces
    @Named
    public LogEntry getNewLogEntry() {
        return logEntry;
    }

    /**
     * Saves new log entry to the persistence store
     * 
     * @throws Exception
     */
    public void create() throws Exception {
        log.info("Creating " + logEntry.getContent());
        logEntryDao.createLogEntry(this.logEntry);
        logEntryEventSrc.fire(logEntry);
        initNewMember();
    }

    /**
     * Prepares new log entry object for the view
     */
    @PostConstruct
    public void initNewMember() {
        logEntry = new LogEntry();
    }
}
