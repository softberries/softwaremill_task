package com.sm.task.smee.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.jboss.seam.solder.logging.Category;

import com.sm.task.smee.model.LogEntry;

/**
 * Log entry SBS - Session Based Store
 * This class simulates the database backend
 * 
 * @author kris
 *
 */
@SessionScoped
public class LogEntrySBS implements Serializable {

	private static final long serialVersionUID = -7986749668603569898L;

	private List<LogEntry> entries;
	
	@Inject
    @Category("smee")
    private Logger log;
	
	public List<LogEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<LogEntry> entries) {
		this.entries = entries;
	}
	/**
	 * Initialize empty list for our log entries on 
	 * each session
	 */
	@PostConstruct
	public void init(){
		log.info("post construct in sdb");
		entries = new ArrayList<LogEntry>();
	}

}
