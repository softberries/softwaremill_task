package com.sm.task.smee.dao;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.jboss.seam.solder.logging.Category;

import com.sm.task.smee.model.LogEntry;

/**
 * Simulates data access layer
 * This bean would be probably a Stateless EJB when using a full profile J2EE server
 * 
 * @author kris
 *
 */
@RequestScoped
public class LogEntryDao {

	/**
	 * Object simulating database
	 */
	@Inject
	private LogEntrySBS database;
	
	@Inject
    @Category("smee")
    private Logger log;
	
	/**
	 * DAO type method for saving log entries
	 * 
	 * @param entry  new log entry to save
	 */
	public void createLogEntry(LogEntry entry){
		//see on the console that injected database object is the same (session scoped bean)
		log.info("datatabase: " + database);
		
		//don't look at the line below, its just a simple id generation :)
		entry.setId((long) database.getEntries().size());
		
		database.getEntries().add(entry);
	}
	/**
	 * Get all log entries from persistent store
	 * @return
	 */
	public List<LogEntry> getAllLogEntries(){
		log.info("Entries: " + database.getEntries());
		return database.getEntries();
	}
}
