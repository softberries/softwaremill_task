package com.sm.task.smee.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.metadata.ConstraintDescriptor;

import org.apache.commons.lang.RandomStringUtils;
import org.jboss.arquillian.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.sm.task.smee.controller.LogEntryBean;
import com.sm.task.smee.model.LogEntry;

@RunWith(Arquillian.class)
public class LogEntryBeanTest {

	private Validator validator;

	@BeforeClass
	public static void setUp() {
		
	}

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true, "com.sm.task.smee.controller")
				.addPackages(true, "com.sm.task.smee.dao")
				.addPackages(true, "com.sm.task.smee.data")
				.addPackages(true, "com.sm.task.smee.model")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	LogEntryBean logEntryCreator;

	@Inject
	Logger log;

	@Test
	public void testRegister() throws Exception {
		LogEntry logEntry = logEntryCreator.getNewLogEntry();
		logEntry.setContent("some test content");
		logEntryCreator.create();
		assertNotNull(logEntry.getId());
		log.info(logEntry.getContent() + " was persisted with id "
				+ logEntry.getId());
	}

	@Test
	public void shouldFailOnMaximumLengthError() throws Exception {
		//given
		LogEntry logEntry = logEntryCreator.getNewLogEntry();
		String content = RandomStringUtils.random(161);
		//make sure its longer than 160 chars
		assertTrue(content.length() > 160);
		logEntry.setContent(content);
		
		//when
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		Set<ConstraintViolation<LogEntry>> constraintViolations = validator.validate(logEntry);
		
		//then
		assertEquals(1, constraintViolations.size());
		assertEquals("Maximum 160 chars", constraintViolations.iterator().next().getMessage());
	}
	@Test
	public void shouldFailOnEmptyContentError() throws Exception {
		//given
		LogEntry logEntry = logEntryCreator.getNewLogEntry();
		String content = "";
		logEntry.setContent(content);
		
		//when
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		Set<ConstraintViolation<LogEntry>> constraintViolations = validator.validate(logEntry);

		//then
		assertEquals(2, constraintViolations.size());
		assertEquals("Content for Log Entry cannot be empty", constraintViolations.iterator().next().getMessage());
	}
	
	@Test
	public void shouldFailOnEmptyContentErrorWhenNullGiven() throws Exception {
		//given
		LogEntry logEntry = logEntryCreator.getNewLogEntry();
		String content = null;
		logEntry.setContent(content);
		
		//when
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		Set<ConstraintViolation<LogEntry>> constraintViolations = validator.validate(logEntry);

		//then
		assertEquals(1, constraintViolations.size());
		assertEquals("Content for Log Entry cannot be empty", constraintViolations.iterator().next().getMessage());
	}

	@Produces
	public Logger produceLog(InjectionPoint injectionPoint) {
		return Logger.getLogger(injectionPoint.getMember().getDeclaringClass());
	}

	/**
	 * Utility method, with more then one unit test class I would move it to another utility object
	 * Leaving it here for reduced cluttering
	 * @param violations
	 * @param constraint
	 * @return
	 */
	public static <T> boolean containsConstraintViolation(
			Set<ConstraintViolation<T>> violations, Class<?> constraint) {

		for (ConstraintViolation<?> violation : violations) {
			ConstraintDescriptor<?> descriptor = violation
					.getConstraintDescriptor();
			if (constraint.isAssignableFrom(descriptor.getAnnotation()
					.getClass()))
				return true;
		}
		return false;
	}

}
