#!/bin/bash

#another very long and much more sophisticated bash script for deploying earlier build project to JBoss container

echo "Your JBOSS_HOME is set to: "  $JBOSS_HOME
echo "Deploying application to: " $JBOSS_HOME/standalone/deployments/
cp target/smee.war $JBOSS_HOME/standalone/deployments/


URL="http://localhost:8080/smee/"

echo "Trying to start the web browser at: "$URL

if which xdg-open > /dev/null
then
  xdg-open $URL
elif which gnome-open > /dev/null
then
  gnome-open $URL
fi

echo "Finished"

